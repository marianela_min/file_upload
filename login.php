<?php // Do not put any HTML above this line

require_once "ids.php";


if ( isset($_POST['cancel'] ) ) {
    // Redirect the browser to login.php
    header("Location: login.php");
    return;
}



$failure = false;  // If we have no POST data

// Check to see if we have some POST data, if we do process it
if ( isset($_POST['who']) ) {
    if ( strlen($_POST['who']) != 6) {
        $failure = "6-digit-number is required";
    } else {
        $var = urlencode($_POST['who']);
            // echo $var."<br />";
        $i = 0;
        foreach ($ids as $id) {
            // echo $id."<br />";
            $i++;
            // echo $i;
            if ($id == $var) {
                // echo "Move to the page and start uploading...";
                if(isset($_POST['name']) && strlen($_POST['name'])>=3 )
                    {header("Location: upload.php?name=".urlencode($_POST['name'])."&id=".urldecode($_POST['who']));
                }else{
                    // echo "number of array: ".$i."id: ".$id."  name: ".$id_names[$i-1];
                    // echo '<br/>';
                    header("Location: upload.php?name=".$nameFromId[$var]."&id=".$var);
                    }
            return;
            }else
            {
               $failure = "Incorrect number"; 
            }
        }
      } 
    }


// Fall through into the View
?>

<!DOCTYPE html>
<html>
<head>
<?php require_once "bootstrap.php";
        require_once "ids.php";
 ?>
<title>Login Page - Marianela Mendoza</title>
</head>
<body>
<div class="container">
<h1>Lets polish what you have</h1>
<h4>Please Log In</h4>
<?php
// Note triple not equals and think how badly double
// not equals would work here...
if ( $failure !== false ) {
    // Look closely at the use of single and double quotes
    echo('<p style="color: red;">'.htmlentities($failure)."</p>\n");
}
?>

<p>
    Please login to upload your CV. Make sure you have your 6-digit number to log in. 
</p>

<form method="POST">
<label for="nam">Name: </label>
<input type="text" name="name" id="nam"><br/>
<label for="id">6-digit Number: </label>
<input type="text" name="who" id="id"><br/>
<!-- <label for="id_1723">Password </label> -->
<!-- <input type="password" name="pass" id="id_1723"><br/> -->
<!-- <label for="id_1723">LinkedIn account </label> -->
<!-- <input type="text" name="linkedIn account"> -->
<input type="submit" value="Log In">
<input type="submit" name="cancel" value="Cancel">
</form>
<p>
For a password hint, view source and find a password hint
in the HTML comments.
<!-- Hint: The 6-digit number is your student ID or
the 6 digit number you gave me on the whatsapp group chat.-->
</p>
</div>
</body>
</html>
<?php 

require_once "phpFileUploadErrors.php";

	if(isset($_GET['id'])){
		$id=$_GET['id'];
		if(isset($_GET['name']))
			$student = 	$_GET['name'];
		else
			$student = "Student";
		// echo $id;
	}else
	die;
	
$failure = false;
$error="0";

	if(isset($_FILES['userfile']))
	{
		// print_r($_FILES);
		// echo "<br />";
		// pre_r($_FILES);

		$error = $_FILES['userfile']['error'];

		// check for allowed extensions
		$ext_error = false;
		$extensions = array('pdf');
		$file_ext = explode('.',$_FILES['userfile']['name']); //get the extension as pdf
		$file_ext = end($file_ext);
		if(!in_array($file_ext,$extensions))
		{
			$ext_error=true;
		}

		//if error of upload is not 0
		if($error)
		{
			$failure = $phpFileUploadErrors[$_FILES['userfile']['error']];
			// echo $failure;
		}
		elseif ($ext_error){
			$failure =  "Invalid file extension";
			// echo $failure;
		}
		else
		{
			// echo "Success!  Image has been uploaded.";
			move_uploaded_file($_FILES['userfile']['tmp_name'], "cv/id_".$id."_".$_FILES['userfile']['name']);
		}

		
		
		// echo $error;
	}


	function pre_r($array){
		echo "<pre>";
		print_r($array);
		echo "</pre>";
	}
 ?>




<!DOCTYPE html>
<html>
<head>
	<title>PHP file upload</title>
	<?php
	require_once "bootstrap.php";
	?>
</head>
<body>
<div class="container">
<h1>Lets polish what you have</h1>
<p> Hi <?php echo $student ?> </p>
<p> Limit your file to no more than 2MB. Otherwise It wont be uploaded.</p>
<?php

if ( $failure !== false ) {
    // Look closely at the use of single and double quotes
    echo('<p style="color: red;">'.htmlentities($failure)."</p>\n");
    echo('<p style="color: red;"> We could not upload your document.<br /> If you encounter several problems, please <a href="mailto:mmendozamend@neiu.edu">contact me</a>.</p>');
}
else{
	if(isset($_FILES['userfile']))
		echo('<p style="color: green;"> Document Uploaded successfully. </p>');
}

?>

<form action="" method="POST" enctype="multipart/form-data">
	<label for="fol">File to upload: </label>
	<input type="file" name="userfile" id="fol" />
	<!-- <label for="lnkin">LinkedIn Account: </label> -->
	<!-- <input type="text" name="lnkin" id="lnkin" /> -->
	<br />
	<input type="submit" value="submit">
</form>
<br />
<p>	If you want to cancel, go ahead.</p>
<button><a href="login.php">Cancel</a></button>
	

</div>
</body>
</html>